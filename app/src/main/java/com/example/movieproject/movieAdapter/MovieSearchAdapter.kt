package com.example.movieproject.movieAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieproject.MovieResponseData
import com.example.movieproject.R

class MovieSearchAdapter(
    private val searchDataList: ArrayList<MovieResponseData>,
    private val context: Context,
) :
    RecyclerView.Adapter<MovieSearchAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var searchPoster: ImageView = itemView.findViewById(R.id.imageView_search_result)
        var searchTitle: TextView = itemView.findViewById(R.id.textView_search_result)
//        var searchDesc: TextView = itemView.findViewById(R.id.textView_search_result)

        init {
            itemView.setOnClickListener { v: View ->
                val position = this.adapterPosition
                Toast.makeText(
                    itemView.context,
                    "You clicked on item : ${position + 1}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.movie_search_result_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(searchDataList[position].moviePoster).into(holder.searchPoster)
        holder.searchTitle.text = searchDataList[position].movieTitle
//        holder.searchDesc.text = searchDataList[position].movieDescription
    }

    override fun getItemCount(): Int {
        return searchDataList.size
    }
}