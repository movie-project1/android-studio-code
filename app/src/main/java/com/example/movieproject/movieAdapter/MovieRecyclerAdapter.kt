package com.example.movieproject.movieAdapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieproject.MovieResponseData
import com.example.movieproject.R
import java.lang.StringBuilder

class MovieRecyclerAdapter(
    private val movieDataList: ArrayList<MovieResponseData>,
    private val context: Context,
    //test1
    private val listener:ViewHolder.Listener

) :
    RecyclerView.Adapter<MovieRecyclerAdapter.ViewHolder>() {
//    private var timeBtnList = arrayListOf<String>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //test 2
        interface Listener {
            fun getBookingList(list:ArrayList<String>)
        }


        var moviePost: ImageView = itemView.findViewById(R.id.id_movie_imageview)
        var movieTitle: TextView = itemView.findViewById(R.id.id_movie_title)
        var mGenre: TextView = itemView.findViewById(R.id.id_movie_genre)
        init {
            itemView.setOnClickListener { v: View ->
                val position = this.adapterPosition
                Toast.makeText(
                    itemView.context,
                    "You clicked on item : ${position + 1}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.movie_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(movieDataList[position].moviePoster).into(holder.moviePost)
        holder.movieTitle.text = movieDataList[position].movieTitle
        val sb = StringBuilder();
        for (i in 0 until movieDataList[position].movieGenre.size) {
            if (i != movieDataList[position].movieGenre.size - 1) {
                sb.append(movieDataList[position].movieGenre[i].genre + "/ ")
            } else {
                sb.append(movieDataList[position].movieGenre[i].genre)
            }
        }
        holder.mGenre.text = sb
        //test 3
        val bookingListTime:ArrayList<String> = ArrayList()
//        val bookingListTime:ArrayList<String>
        bookingListTime.addAll(movieDataList[position].bookingTime.bookingDate)
        listener.getBookingList(bookingListTime)
    }

    override fun getItemCount(): Int {
        return movieDataList.size
    }
}
