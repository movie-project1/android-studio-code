package com.example.movieproject.movieAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieproject.MovieResponseData
import com.example.movieproject.R

class MovieHomeAdapter(private var recentlyMovie:ArrayList<MovieResponseData>,private val context: Context):RecyclerView.Adapter<MovieHomeAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val movieRecently:ImageView =itemView.findViewById(R.id.recent_release)
            init {
            itemView.setOnClickListener { v: View ->
                val position: Int = adapterPosition
                Toast.makeText(
                    itemView.context,
                    "You clicked on item : ${position + 1}",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val releaseView = LayoutInflater.from(parent.context).inflate(R.layout.recently_released,parent,false)
        return ViewHolder(releaseView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(recentlyMovie[position].moviePoster).into(holder.movieRecently)
    }

    override fun getItemCount(): Int {
        return recentlyMovie.size
    }
}