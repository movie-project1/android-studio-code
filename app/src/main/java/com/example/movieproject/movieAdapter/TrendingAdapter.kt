package com.example.movieproject.movieAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.movieproject.R

class TrendingAdapter(private var trendingMovie:List<Int>):RecyclerView.Adapter<TrendingAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val movieTrending:ImageView= itemView.findViewById(R.id.trending_poster)
        init {
            itemView.setOnClickListener { v: View ->
                val position: Int = adapterPosition
                Toast.makeText(
                    itemView.context,
                    "You clicked on item : ${position + 1}",
                    Toast.LENGTH_SHORT
                ).show()
        }

    }}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val trendingView = LayoutInflater.from(parent.context).inflate(R.layout.activity_trending,parent,false)
        return ViewHolder(trendingView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.movieTrending.setImageResource(trendingMovie[position])
    }

    override fun getItemCount(): Int {
        return trendingMovie.size
    }
}