package com.example.movieproject.movieAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.movieproject.R
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BookingTimeAdapter(
//    private val timeBtnList: ArrayList<MovieResponseData>,
    private val timeBtnList: ArrayList<String>,
    private val context: Context
) : RecyclerView.Adapter<BookingTimeAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val timeBooking: Button = itemView.findViewById(R.id.booking_time)

        init {
            timeBooking.setOnClickListener { v: View ->
                val position = this.adapterPosition
                Toast.makeText(
                    itemView.context,
                    "You clicked on item : ${position + 1}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bookingBt =
            LayoutInflater.from(parent.context).inflate(R.layout.time_booking_layout, parent, false)
        return ViewHolder(bookingBt)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.timeBooking.text = position.toString()
//        val listDateSize = timeBtnList[position].bookingTime.bookingDate.size

//        println(timeBtnList[position])
//        for (i in 0 until listDateSize){
//            val sdate = convertDate(timeBtnList[position].bookingTime.bookingDate[i])
//            holder.timeBooking.text = sdate
//        }
        val sdate = convertDate(timeBtnList[position])
        holder.timeBooking.text = sdate

//        println(timeBtnList[position])
    }

    override fun getItemCount(): Int {
        return timeBtnList.size
    }

    private fun convertDate(jsDate: String): String {
//        try {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH)
        val decFormatter = DecimalFormat("00")
        val kDate = formatter.parse(jsDate)
        val cal = Calendar.getInstance()
        cal.time = kDate
        val setBookTime = "${decFormatter.format(cal.get(Calendar.HOUR_OF_DAY))}:${
            decFormatter.format(
                cal.get(Calendar.MINUTE)
            )
        }"

//        } catch (err: Exception) {
//            println(err)
//        }
        return setBookTime
    }
}