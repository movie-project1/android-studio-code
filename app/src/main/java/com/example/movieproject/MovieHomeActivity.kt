package com.example.movieproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieproject.movieAdapter.MovieHomeAdapter
import com.example.movieproject.movieAdapter.TrendingAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieHomeActivity : AppCompatActivity() {
    private var trendingList = mutableListOf<Int>()
    private var releasedList = arrayListOf<MovieResponseData>()
    private var releasedAdapter: MovieHomeAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        postTrend()
        getRecentReleased()
        val trendingRecyclerView = findViewById<RecyclerView>(R.id.trending_recycler)
        trendingRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        trendingRecyclerView.adapter = TrendingAdapter(trendingList)
        val releasedRecyclerView = findViewById<RecyclerView>(R.id.release_recycler)
        releasedAdapter = MovieHomeAdapter(releasedList,this)
        releasedRecyclerView.layoutManager =
            GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        releasedRecyclerView.adapter = releasedAdapter
    }

    private fun trendList(trending: Int) {
        trendingList.add(trending)
    }

    private fun postTrend() {
        for (i in 1..5) {
            trendList(R.drawable.rayalastdragonposter1joblo_1_400x600)
        }
    }

    private fun getRecentReleased() {
        val call: Call<PlaceholderMovie> = ApiClient.getClient.getSearchMovie("","asc","createdAt")
        call.enqueue(object : Callback<PlaceholderMovie> {
            override fun onResponse(
                call: Call<PlaceholderMovie>,
                response: Response<PlaceholderMovie>
            ) {
                releasedList.addAll(response.body()?.resData ?: arrayListOf())
                releasedAdapter!!. notifyDataSetChanged ()
            }

            override fun onFailure(
                call: Call<PlaceholderMovie>,
                t: Throwable
            ) {
                println("error" + t.message)
            }
        })
    }
}