package com.example.movieproject


import com.google.gson.annotations.SerializedName
import kotlin.collections.ArrayList

class PlaceholderMovie(
    @SerializedName("data")
    var resData: ArrayList<MovieResponseData>
)

class MovieResponseData(
    @SerializedName("booking_time")
    var bookingTime: MovieDate,
    @SerializedName("title")
    var movieTitle: String,
    @SerializedName("poster")
    var moviePoster:String,
    @SerializedName("description")
    var movieDescription: String,
    @SerializedName("_id")
    private var movieId: String,
    @SerializedName("movie_genre")
    var movieGenre: ArrayList<MovieGenre>,
)


class MovieDate(
    @SerializedName("date")
    var bookingDate: ArrayList<String>
)

class MovieGenre(
    @SerializedName("genreName")
     var genre: String
)