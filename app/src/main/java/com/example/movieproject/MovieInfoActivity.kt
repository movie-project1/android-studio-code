package com.example.movieproject

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieproject.movieAdapter.BookingTimeAdapter
import com.example.movieproject.movieAdapter.MovieRecyclerAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MovieInfoActivity : AppCompatActivity() {
    //movie variable
    private var movieDataList = arrayListOf<MovieResponseData>()
    private var movieDataAdapter: MovieRecyclerAdapter? = null
    //time booking list
//    private var timeBtnList = arrayListOf<MovieResponseData>()
    private var timeBtnList = arrayListOf<String>()
    private var timeAdapter:BookingTimeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_info)
        val movieRecyclerView = findViewById<RecyclerView>(R.id.id_movie_recyclerview)
        val desc = findViewById<TextView>(R.id.movie_description_id)
        val book = findViewById<Button>(R.id.booking_time)
        val timeBtnView = findViewById<RecyclerView>(R.id.btn_recyclerView)

        //movieRecyclerView
        movieDataAdapter =
            MovieRecyclerAdapter(movieDataList, this,object : MovieRecyclerAdapter.ViewHolder.Listener{
                override fun getBookingList(list: ArrayList<String>){
                    timeBtnList.addAll(list)

                }
            })
        movieRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        movieRecyclerView.adapter = movieDataAdapter
        //BookRecycleView
        timeAdapter = BookingTimeAdapter(timeBtnList,this)
        timeBtnView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        timeBtnView.adapter = timeAdapter
        getMovieData()

        movieRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val currentMovie =
                    (movieRecyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                var firstMovie =
                    (movieRecyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                if (firstMovie < 0) {
                    firstMovie =
                        (movieRecyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                }
                //do something firstMovie
                if (movieDataList.size > firstMovie) {
                    val data = movieDataList[firstMovie]
                    desc.text = data.movieDescription
                    timeBtnView.adapter = timeAdapter
                }

            }
        })
    }
    private fun getMovieData() {
        val call: Call<PlaceholderMovie> = ApiClient.getClient.getMovie()
        call.enqueue(object : Callback<PlaceholderMovie> {
            override fun onResponse(
                call: Call<PlaceholderMovie>,
                response: Response<PlaceholderMovie>
            ) {
                movieDataList.addAll(response.body()?.resData ?: arrayListOf())
//                timeBtnList.addAll(movieDataList)
                movieDataAdapter!!.notifyDataSetChanged()
//                timeAdapter!!.notifyDataSetChanged()
            }

            override fun onFailure(
                call: Call<PlaceholderMovie>,
                t: Throwable
            ) {
                println("error" + t.message)
            }
        })
    }
}
