package com.example.movieproject.models

class TrendingModel {
    private  var movie_poster = mutableListOf<Int>()
    private var movie_title = mutableListOf<String>()
    private var movie_description = mutableListOf<String>()

    fun TrendingModel(movie_poster:Int, movie_title:String, movie_description:String){
        this.movie_poster.add(movie_poster)
        this.movie_title.add(movie_title)
        this.movie_description.add(movie_description)
    }
    fun getMoviePoster(): MutableList<Int> {
        return movie_poster
    }
    fun getMovieTitle():MutableList<String>{
        return movie_title;
    }
    fun getMovieDescription():MutableList<String>{
        return movie_description
    }

}