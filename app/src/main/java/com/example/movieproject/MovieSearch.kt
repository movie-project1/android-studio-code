package com.example.movieproject

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieproject.movieAdapter.MovieSearchAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieSearch : AppCompatActivity() {
    private var searchDataList = arrayListOf<MovieResponseData>()
    private var searchDataAdapter: MovieSearchAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_search_activity)
        val sortActAdapter = ArrayAdapter(
            this,
            R.layout.dropdown_item,
            getFilterParams()
        )

        val sortACT = findViewById<AutoCompleteTextView>(R.id.id_sort_act)
        sortACT.setAdapter(sortActAdapter)
        sortACT.setOnItemClickListener { adapterView, view, position, id ->
            when (position) {
                0 -> {
                    setResultViews("asc", "createdAt")
                }
                1 -> {
                    setResultViews("desc", "createdAt")
                }
                2 -> {
                    setResultViews("desc", "title")
                }
            }
        }
        showFoundMovies()
    }

    private fun setResultViews(sortParams: String, sortBy: String) {
        val searchText = findViewById<EditText>(R.id.search_movie)
        val keyWord = searchText.text.toString()
        val movieSearchView = findViewById<RecyclerView>(R.id.view_search_result)
        searchDataAdapter = MovieSearchAdapter(searchDataList, this)
        movieSearchView.layoutManager =
            GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        movieSearchView.adapter = searchDataAdapter
        getSearchData(keyWord, sortParams, sortBy)
        searchDataList.clear()
    }

    private fun showFoundMovies() {
        val searchButton = findViewById<Button>(R.id.search_button)
        searchButton.setOnClickListener {
            setResultViews("asc", "title")
        }
    }

    private fun getFilterParams(): ArrayList<String> {
        val filterList = ArrayList<String>()
        filterList.add("Asc Created")
        filterList.add("Desc Created")
        filterList.add("Desc Title")
        return filterList
    }

    private fun getSearchData(keyWord: String, sortParams: String, sortBy: String) {

        val call: Call<PlaceholderMovie> =
            ApiClient.getClient.getSearchMovie(keyWord, sortParams, sortBy)
        call.enqueue(object : Callback<PlaceholderMovie> {
            override fun onResponse(
                call: Call<PlaceholderMovie>,
                response: Response<PlaceholderMovie>
            ) {
                searchDataList.addAll(response.body()?.resData ?: arrayListOf())
                searchDataAdapter!!.notifyDataSetChanged()
            }

            override fun onFailure(
                call: Call<PlaceholderMovie>,
                t: Throwable
            ) {
                println("error" + t.message)
            }
        })
    }
}