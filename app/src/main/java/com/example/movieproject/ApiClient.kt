package com.example.movieproject

import com.example.movieproject.client.MovieApi
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient{
    var base_URL: String = "https://projectmovie.gabatch11.my.id/"
    val getClient: MovieApi
        get() {
            val gson = GsonBuilder().setLenient().create()
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(base_URL).client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()
            return retrofit.create(MovieApi::class.java)
        }
}