package com.example.movieproject.client

import com.example.movieproject.MovieDate
import com.example.movieproject.PlaceholderMovie
import retrofit2.Call;
import retrofit2.http.*

interface MovieApi {

    @GET("movie")
    fun  getMovie():Call<PlaceholderMovie>
    @GET("movie/sort")
    fun  getRecentReleased():Call<PlaceholderMovie>
    @GET("movie/search/")
    fun getSearchMovie(@Query("keywords") keywords:String,@Query ("sortParams") sortParams: String, @Query ("sortBy") sortBy: String):Call<PlaceholderMovie>
}