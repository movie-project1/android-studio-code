package com.example.movieproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val registerText = findViewById<TextView>(R.id.loginNeedRegister)
        registerText.setOnClickListener{
            val intent = Intent(this,MovieSearch::class.java)
            startActivity(intent)
        }
        val loginProc = findViewById<Button>(R.id.loginButton)
        loginProc.setOnClickListener{
            val intent = Intent(this,MovieInfoActivity::class.java)
            startActivity(intent)
        }
        val resetPass = findViewById<TextView>(R.id.resetPassword)
        resetPass.setOnClickListener{
            val intent = Intent(this,MovieHomeActivity::class.java)
            startActivity(intent)
        }
    }

}